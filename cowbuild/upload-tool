#!/usr/bin/env python

import argparse
import os
import sys
import subprocess
from ConfigParser import RawConfigParser
import paramiko

class Changes():
    def __init__(self, changes):
        self.changes = changes
        self.section = None
        self.files = []
        self.parser()

    def parser(self):
        with open(self.changes) as fp:
            parser = False
            for line in fp.readlines():
                if line.endswith('\n'):
                    line = line[:-1]
                if line.startswith('Files:'):
                    parser = True
                    continue
                if parser and line.startswith(' '):
                    if self.section is None:
                        self.section = line.split()[2]
                    self.files.append(line.split()[4])
                    continue
                if parser and not line.startswith(" "):
                    parsre = False
                    continue

class Cache():
    def __init__(self, cache_path):
        self.cache_path = cache_path

    def results(self, binary_only):
        results = []
        section = None
        caches = []
        for dir, _, files in os.walk(self.cache_path):
            for file in files:
                if file.endswith(".changes"):
                    file_path = os.path.join(cache_path, dir, file)
                    changes = Changes(file_path)
                    section = changes.section
                    for f in changes.files:
                        result = os.path.join(dir, f)
                        if f not in caches:
                            caches.append(f)
                            if result.endswith(".buildinfo"):
                                continue
                            if binary_only:
                                if result.endswith(".deb"):
                                    results.append(result)
                            else:
                                results.append(result)
        return section, results

            

    def execute(self, command, output):
        os.environ['DIST'] = self.dist
        os.environ['ARCH'] = self.arch
        if output:
            p = subprocess.Popen(command, shell=False)
        else:
            FNULL = open("/dev/null", "w")
            p = subprocess.Popen(command, shell=False, stdout=FNULL, stderr=FNULL)
        retcode = p.wait()
        if not output:
            FNULL.close()
        return retcode

# arguments parser
parser = argparse.ArgumentParser()
parser.add_argument("--quiet", action="store_true", default=False)
parser.add_argument("--binary-only", action="store_true", default=False)
parser.add_argument("--reponame", type=str)
parser.add_argument("--upload", type=str)
parser.add_argument("--config", type=str, default="upload.conf")
parser.add_argument("--clean", action="store_true")
args = parser.parse_args()

cache_path = os.path.abspath(os.path.join(os.getcwd(), args.upload))
if not os.path.exists(cache_path):
    print("E: %s is not exists" % cache_path)
    sys.exit(1)

if not os.path.exists(args.config):
    print("E: %s is not exists" % args.config)
    sys.exit(1)

try:
    cf = RawConfigParser()
    cf.read(args.config)
    if args.reponame not in cf.sections():
        print('E: %s is not configed' % args.reponame)
        sys.exit(1)
except Exception as e:
    print('E: exception raised. %s' % e)
    sys.exit(1)


cache = Cache(cache_path)
section = None
_section, results = cache.results(args.binary_only)

if _section is not None:
    for sec in ['main', 'contrib', 'non-free']:
        if sec in _section:
            section = sec
            break

if section is None:
    section = 'main'

trans = paramiko.Transport((cf.get(args.reponame, 'fqdn')))
trans.connect(username = cf.get(args.reponame, 'login'), password = cf.get(args.reponame, 'password'))
sftp = paramiko.SFTPClient.from_transport(trans)
remotepath = cf.get(args.reponame, 'remotepath')
print("I: upload packages")
dsc = None
debs = []
for result in results:
    basename = os.path.basename(result)
    print(" uploading %s" % basename)
    sftp.put(result, os.path.join(remotepath, basename))

    if basename.endswith(".dsc"):
        dsc = result
    if basename.endswith(".deb"):
        debs.append(result)
trans.close()

ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh.connect(cf.get(args.reponame, 'fqdn'),
        22,
        cf.get(args.reponame, 'login'), 
        cf.get(args.reponame, 'password')
        )
if dsc is not None:
    print("I: include dsc file")
    basename = os.path.basename(dsc)
    dscfile = os.path.join(remotepath, basename)
    command = cf.get(args.reponame, 'includedsc') % (
            {'section': section,
             'dscfile': dscfile
            })
    _, stdout, stderr = ssh.exec_command(command)
    for line in stdout.readlines():
        print(" %s" % line[:-1])
    for line in stderr.readlines():
        print(" %s" % line[:-1])

if len(debs) != 0:
    print("I: include deb file")
    debfiles = []
    for deb in debs:
        basename = os.path.basename(deb)
        debfiles.append(os.path.join(remotepath, basename))

    command = cf.get(args.reponame, 'includedeb') % ({
                'section': section,
                'debfiles': " ".join(debfiles)
            })
    _, stdout, stderr = ssh.exec_command(command)
    for line in stdout.readlines():
        print(" %s" % line[:-1])
    for line in stderr.readlines():
        print(" %s" % line[:-1])

try:
    command = cf.get(args.reponame, 'post_commands') % ({
            'reponame': args.reponame
            })
except Exception as e:
    command = None

if command:
    _, stdout, stderr = ssh.exec_command(command)
    for line in stdout.readlines():
        print(" %s" % line[:-1])

ssh.close()
