#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
import subprocess
import os
from distutils.version import LooseVersion

from debian import debfile
import collections
import io
import gzip


def try_run_command(command):
    try:
        subprocess.check_call(command)
        return True
    except Exception as e:
        print("Execution of [%s] Failed: %s" % (command, e))
        return False

def check_if_btrfs(mount_point):
    with open('/proc/mounts') as fp:
        for line in fp.readlines():
            if line.split()[1] == mount_point:
                if line.split()[2] == 'btrfs':
                    return True
                else:
                    return False
        return False

def _btrfs_subvolume(args):
    _command = ['sudo', 'btrfs', 'subvolume' ] + args
    return try_run_command(_command)

def create_subvolume(snapshot):
    return _btrfs_subvolume(['create', snapshot])

def delete_subvolume(snapshot):
    return _btrfs_subvolume(['delete', snapshot])

def snapshot_subvolume(source, destination,  readonly=True):
    if os.path.exists(destination):
        return False
    if not os.path.exists(source):
        return False
    if readonly:
        return _btrfs_subvolume(['snapshot', '-r', source, destination])
    else:
        return _btrfs_subvolume(['snapshot', source, destination])

def create_symlink(path, source, destination):
    if os.path.lexists(os.path.join(path, destination)):
        os.unlink(os.path.join(path, destination))
    return subprocess.check_call(['sudo', 'ln', '-s', source, destination], cwd = path)

def debian_dists(path):
    '''
    Get the mirror list from path
    '''
    for root, dirs, _ in os.walk(path):
        if not os.path.isdir(path):
            break
        if 'pool' in dirs and 'dists' in dirs:
            yield root
            break

def repo_diff(to_desc, from_desc=None):
    """
    origin_desc and dest_desc are like dists/unstable/main/binary-amd64/Packages.gz
    """
    def parse(desc):
        result = {}
        with gzip.open(desc, 'rt') as gf:
            for line in gf.readlines():
                flag = False
                if line.startswith("Package: "):
                    p = line.strip().replace("Package: ","")
                if line.startswith("Version: "):
                    v = line.strip().replace("Version: ","")
                    flag = True
                if flag:
                    result[p] = str(v)
        return result

    to_result = parse(to_desc)
    if from_desc is None:
        from_result = {}
    else:
        from_result = parse(from_desc)

    # addnew and remove is like : {'nautilus': '3.20'}
    addnew = {}
    remove = {}
    # different can be upgrade or downgrade
    # and like {'nautilus':{'from':'3.20.1', 'to': '3.22.2'}} 
    different = {}

    for pkg, version in to_result.items():
        if pkg not in from_result:
            addnew[pkg] = version
        else:
            if from_result[pkg] == version:
                pass
            else:
                _version = {'from': from_result[pkg], 'to': version}
                different[pkg] = _version

    for pkg, version in from_result.items():
        if pkg not in to_result:
            remove[pkg] = version
    return {'addnew': addnew, 'different': different, 'remove': remove}

def repo_diff_string(desc):

    result = ""

    _addnew = desc['addnew']
    for pkg in _addnew:
        result += "++ %s %s\n" % (pkg, _addnew[pkg])

    _different = desc['different']
    for pkg in _different:
        result += "+- %s %s %s\n" % (pkg, _different[pkg]['from'], _different[pkg]['to'])

    _remove = desc['remove']
    for pkg in _remove:
        result += "-- %s %s\n" % (pkg, _remove[pkg])

    return result

def parse_urgency(deb_file, from_version=None, to_version=None):
    deb = debfile.DebFile(deb_file)
    _tgz = deb.changelog()

    return parse_changelog_urgency(_tgz, from_version, to_version)

def get_changelog(tar, from_version=None, to_version=None):
    if tar is None:
        return None
    import tarfile
    from debian.changelog import Changelog
    extension = os.path.splitext(tar)[1][1:]
    fp = open(tar, 'rb')
    if extension == 'xz':
        import subprocess
        import signal

        proc = subprocess.Popen(['unxz', '--stdout'],stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                universal_newlines=False,preexec_fn=lambda: signal.signal(signal.SIGPIPE, signal.SIG_DFL))

        data = proc.communicate(fp.read())[0]

        buffer = io.BytesIO(data)
    else:
        buffer = io.BytesIO(fp)

    fp.close()

    _tar = tarfile.open(fileobj=buffer, mode='r:*')

    changelog = None
    for fname in _tar.getnames():
        if 'debian/changelog' in fname:
            changelog = fname
            break
    if changelog is None: 
        print("%s is not support" % tar)
        return None
    changelog_obj = _tar.extractfile(changelog)
    content = Changelog(changelog_obj)
    return parse_changelog_urgency(content, from_version, to_version)


def parse_changelog_urgency(changelog, from_version=None, to_version=None):
    result = collections.OrderedDict()
    _flag = False
    if from_version is None:
        _flag = True

    for block in changelog:
        _version = block.version.full_version
        if _version == to_version:
            _flag = True

        if _flag is True:
            if block.version == from_version:
                _flag = False
            elif from_version and LooseVersion(str(block.version)) < LooseVersion(str(from_version)):
                _flag = False

        if _flag:
            result[_version] = block.urgency
    return result

def get_debian_tar_url(repo_path, src_name, version, sec):
    '''
    Get package debian tarball if exists debian.tar.* else
    return the origin tarball else
    return None
    '''

    # May the version is 2:1.2.1-1, but origin tarball is like 1.2.1-1
    if ':' in version:
        version = version.split(':')[1]

    if '-' in version:
        prefix = src_name + '_' + version + '.debian.tar'
    else:
        prefix = src_name + '_' + version + '.tar'

    if src_name.startswith('lib'):
        find = os.path.join("lib%s" % src_name[3], src_name)
    else:
        find = os.path.join(src_name[0], src_name)
    find_path = os.path.join(repo_path, 'pool', sec, find)
    if not os.path.isdir(find_path):
        return None
    for p in os.listdir(find_path):
        if p.startswith(prefix):
            return os.path.join(find_path, p)
    return None
