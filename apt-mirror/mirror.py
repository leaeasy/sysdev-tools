#! /usr/bin/env python2
# -*- coding: utf-8 -*-
#
import helper
import subprocess
import copy
import shutil
import glob

import os
import datetime

class BtrfsException(Exception):
    pass

class BtrfsVolume():
    def __init__(self, path, subvolume='apt-mirror'):
        if not helper.check_if_btrfs(path):
            raise BtrfsException("%s path is not a btrfs mountpoint" % path)
        self.path = path
        self.subvolume = subvolume
        self.base_path = os.path.join(self.path, subvolume)
        self.mirror_list = os.path.join(self.base_path, 'config', 'mirror.list')

        self.orig_mirror_path = None 
        self.dest_mirror_path = None

        self.base_mirror_path = os.path.join(self.base_path, 'mirror')
        self.pdiff_path = os.path.join(self.base_path, 'pdiff')

        self.init()

    def init(self):
        if not os.path.exists(os.path.join(self.base_path)):
            helper.create_subvolume(self.base_path)

        if not os.path.exists(os.path.join(self.base_path, 'mirror')):
            helper.create_subvolume(os.path.join(self.base_path, 'mirror'))

        if not os.path.exists(os.path.join(self.base_path, 'config')):
            helper.create_subvolume(os.path.join(self.base_path, 'config'))

        if not os.path.exists(os.path.join(self.base_path, 'pdiff')):
            helper.create_subvolume(os.path.join(self.base_path, 'pdiff'))
        
        if not os.path.exists(self.mirror_list):
            self.generate_mirror_list()

    def generate_mirror_list(self):
        with open('mirror.list.template') as fp:
            template = ''.join(fp.readlines())
        _mirror_list = template.format(BASE_PATH=self.base_path)
        with open(self.mirror_list, 'w') as mirror_list:
            mirror_list.write(_mirror_list)
        print("You should edit %s manually" % self.mirror_list)

    def mirror(self, save_diff=True):
        if os.path.exists(os.path.join(self.base_path, 'mirror', '.temporary')):
            print(os.path.join(self.base_path, 'mirror', '.temporary'))
            print("Another proccess is running, exit ....")
            return False

        _mirror_historys = self.list_mirrors()
        _mirror_historys.sort()

        if len(_mirror_historys) == 0:
            print("mirror is blank, now create .temporary immediately")
            helper.create_subvolume(os.path.join(self.base_mirror_path, '.temporary'))
            save_diff=False
        else:
            current_mirror_path = _mirror_historys[-1]
            self.orig_mirror_path = current_mirror_path
            print("Get mirror head %s" % current_mirror_path)
            helper.snapshot_subvolume(os.path.join(self.base_mirror_path, current_mirror_path), os.path.join(self.base_mirror_path, '.temporary'), readonly=False)

        print("Try mirror...")
        update = True
        _now_volume = None
        _first = True
        try:
            while True:
                proc = subprocess.Popen(['apt-mirror', self.mirror_list], stdout=subprocess.PIPE)
                while True:
                    line = proc.stdout.readline()
                    if not line:
                        break
                    _output = line.decode().strip()
                    print(_output)
                    if "0 bytes will be downloaded into archive." in _output:
                        update = False
                retcode = proc.wait()
                _first = False

                if retcode:
                    print("__ -> Mirror failed!")
                    break
                elif not update:
                    print("__ -> Mirror is updated!")
                    break
                else:
                    print("__ -> Do Mirror again")

            if save_diff and not _first:
                for dest_volume in self.list_repos('.temporary'):
                    for orig_volume in self.list_repos(current_mirror_path):
                        if dest_volume.name == orig_volume.name:
                            try:
                                dest_volume.clean_pdiff_dirs()
                                dest_volume.save_urgency_list(orig_volume) 
                                dest_volume.save_repo_diff(orig_volume)
                            except Exception as e:
                                import traceback
                                traceback.print_exc() 
                                print(e)
                                raise(e)

            if not _first and not retcode:
                print("__ -> Mirror sucessful!")
                _now_volume = datetime.datetime.now().strftime("%Y%m%d%H%M")
                helper.snapshot_subvolume(os.path.join(self.base_mirror_path, '.temporary'), os.path.join(self.base_mirror_path, _now_volume), readonly=True)
                #merge pdiff
                self.merge_pdiff(now_volume=_now_volume)
                helper.create_symlink(self.base_mirror_path, _now_volume, 'current')
                self.dest_mirror_path = _now_volume
        except Exception as e:
            print(e)

        finally:
            helper.delete_subvolume(os.path.join(self.base_mirror_path, '.temporary'))
            self.clean_mirror()
            return update, _now_volume

    def list_mirrors(self):
        mirrors =  os.listdir(self.base_mirror_path)
        if 'current' in mirrors:
            mirrors.remove('current')
        mirrors.sort(reverse=True)
        return mirrors

    def merge_pdiff(self, volume_path='.temporary', now_volume=None):
        if now_volume is None:
            now_volume = datetime.datetime.now().strftime("%Y%m%d%H%M")
        for volume in self.list_repos(volume_path):
            _volume_dir = volume.path.replace(os.path.join(self.base_path, 'mirror', volume_path),'')[1:]
            volume_pdiff_dir = os.path.join(self.base_path, 'pdiff', _volume_dir)
            if not os.path.exists(volume_pdiff_dir):
                os.makedirs(volume_pdiff_dir)
            for _ufile in glob.glob(os.path.join(volume.path, '.pdiff', '*.urgency')):
                _file = os.path.basename(_ufile)
                contents = ['%s\n' % now_volume]
                for line in open(_ufile).readlines():
                    contents.append('  %s' % line)
                contents.append('\n\n')
                if os.path.exists(os.path.join(volume_pdiff_dir, _file)):
                    contents+=open(os.path.join(volume_pdiff_dir, _file)).readlines()
                with open(os.path.join(volume_pdiff_dir, _file), 'w') as fp:
                    print(contents)
                    fp.write(''.join(contents))

    def delete_mirror(self, mirror_path):
        if os.path.exists(os.path.join(self.base_mirror_path, mirror_path)):
            return helper.delete_subvolume(os.path.join(self.base_mirror_path, mirror_path))
        return True

    def clean_mirror(self, max_keep=10):
        repos = self.list_mirrors()
        if len(repos) < max_keep + 1:
            return True
        else:
            clean_repos = repos[max_keep:]
            for repo in clean_repos:
                self.delete_mirror(repo)

    def list_repos(self, mirror):
        repos = []
        repo = helper.debian_dists(os.path.join(self.base_mirror_path, mirror))
        for r in repo:
            repos.append(Repository(path=r, timestamp=mirror))
        return repos


class Repository(object):
    def __init__(self, path, name="debian", timestamp=None):
        if name is None:
            self.name = os.path.basename(path)
        else:
            self.name = name

        self.path = path
        self.timestamp = timestamp
        assert os.path.isdir(os.path.join(path, 'dists'))

    def __str__(self):
        return "%s in %s" % (self.name, self.path)

    @property
    def dists(self):
        return os.path.listdir(os.path.join(path, 'dists'))

    @property
    def sources(self):
        result = []
        for root, _, names in os.walk(os.path.join(self.path, 'dists')):
            if 'Sources.gz' in names:
                _file_url = os.path.join(root, 'Sources.gz')
                dist, sec = _file_url.split('dists',-1)[1].split('/')[1:3]
                _temp = {'dist': dist, 'sec': sec, 'file': _file_url } 
                result.append(_temp)
        return result

    def get_urgency(self, src_name, from_version=None, to_version=None, sec='main'):
        debfile = helper.get_debian_tar_url(repo_path=self.path, src_name=src_name, version=to_version, sec=sec)
        return helper.get_changelog(debfile, from_version, to_version)

    def generate_repo_diff(self, repo=None):
        '''
        #TODO
        '''
        result = dict()
        diff = dict()
        for source in self.sources:
            _dist = source.get('dist')
            _sec = source.get('sec')
            _file = source.get('file')
            _diff = dict()
            if repo:
                _repo_sources = repo.sources
                for _source in _repo_sources:
                    if _source.get('dist') == _dist  and _source.get('sec') == _sec:
                        _diff  = helper.repo_diff(_file, _source.get('file'))
            else:
                _diff = helper.repo_diff(_file, None)

            diff[_sec] = _diff.copy()

        for source in self.sources:
            _dist = source.get('dist')
            _sec = source.get('sec')
            if not result.get(_dist):
                result[_dist] = {}
            result[_dist] = diff
        return result

    def save_repo_diff(self, repo=None):
        os.makedirs(os.path.join(self.path, '.pdiff'), exist_ok=True)
        _repo_diff = self.generate_repo_diff(repo)
        for dist, result in _repo_diff.items():
            for sec, diffs in result.items():
                if repo is not None:
                    filename = "%s-%s.rdiff~%s" % (dist, sec, repo.timestamp)
                else:
                    filename = "%s-%s.rdiff" % (dist, sec)

                fp = open(os.path.join(self.path, '.pdiff', filename), 'w')
                diffs_str = helper.repo_diff_string(diffs)
                fp.write(diffs_str)
                fp.close()

    def generate_urgency_list(self, repo, skip_addnew=True, pass_urgency=['low', 'medium']):
        _diffs = {}
        if self.name == repo.name:
            repo_diff = self.generate_repo_diff(repo)
            for dist, diffs in repo_diff.items():
                pkgs=list()
                for sec, diff in diffs.items(): 
                    if not skip_addnew:
                        for pkg, version in diff['addnew'].items():
                            try:
                                for version, urgency in self.get_urgency(src_name=pkg, sec=sec, to_version=version).items():
                                    if urgency not in pass_urgency:
                                        if pkg not in pkgs:
                                            pkgs.append(pkg)
                                        break
                            except:
                                pass

                    for pkg, version in diff['different'].items():
                        try: 
                            for version, urgency in self.get_urgency(src_name=pkg, from_version=version.get('from'), to_version=version.get('to'), sec=sec).items():
                                if urgency not in pass_urgency:
                                    if pkg not in pkgs:
                                        pkgs.append(pkg)
                                    break
                        except:
                            pass
                _diffs[dist] = pkgs
                
        return _diffs

    def save_urgency_list(self, repo=None):
        diffs = self.generate_urgency_list(repo)
        os.makedirs(os.path.join(self.path, '.pdiff'), exist_ok=True)
        for dist, pkgs in diffs.items():
            if len(pkgs) != 0:
                _urgency = os.path.join(self.path,'.pdiff', '%s-%s.urgency' % (self.name, dist))
                with open(_urgency, 'w') as fp:
                    print("Save urgency update to %s" % _urgency)
                    fp.write('\n'.join(pkgs))

    def clean_pdiff_dirs(self):
        if os.path.exists(os.path.join(self.path, '.pdiff')):
            shutil.rmtree(os.path.join(self.path, '.pdiff'))

    def list_packages(self, dist, sec):
        if os.path.exists(os.path.join(self.path,'dists', dist, sec, 'source', 'Sources.gz')):
            return os.path.join(self.path, 'dists', dist, sec, 'source', 'Sources.gz')

if __name__ == "__main__":
    Mirror = BtrfsVolume("/mnt/mirror-snapshot", "debian")
    m = Mirror.list_mirrors()[0]
    print(Mirror.base_mirror_path)
    for i in Mirror.list_repos(m):
        print(i.path)
