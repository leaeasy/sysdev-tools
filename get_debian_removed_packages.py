#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Get debian packages already removed.
import os
import io
import re

class ParseError(Exception):
    def __init__(self, line):
        self._line = line

    def __str__(self):
        return "Could not parse changelog: " + self.line

class PackagesError(Exception):
    pass

class PackageBlock(object):
    def __init__(self):
        pass


EXTS_HANDLER = {'gz': 'gzip -d --stdout',
        'xz': 'unxz --stdout', 
        'bz2': 'bzip -d --stdout'}

class Packages(object):
    def __init__(self, file, endcoding='utf-8'):
        if not os.path.exists(file):
            raise PackagesError("No %s exists" % file)
        self.__file = file

    def __read(self):
        extension = os.path.splitext(self.__file)[1][1:]
        if extension in EXTS_HANDLER:
            try:
                import subprocess
                import signal
                _command = EXTS_HANDLER[extension].split()
                proc = subprocess.Popen(_command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=False,
                        preexec_fn=lambda: signal.signal(signal.SIGPIPE, signal.SIG_DFL))
            except (OSError, ValueError) as e:
                raise PackagesError(e)

            with open(self.__file) as fp:
                data = proc.communicate(fp.read())[0]

            if proc.returncode != 0:
                raise PackagesError("command has failed with code '%s'" % proc.returncode)

            buffer = io.BytesIO(data)
            return buffer

        else:
            raise PackagesError("%s read failed" % self.__file)

    def parse_packages(self):
        buffer = self.__read()
        _dict = {}
        for line in buffer.readlines():
            if line.strip() != "":
                if not line.startswith(" "):
                    key, value = line.strip().split(':', 1)
                    key = key.strip()
                    value = value.strip()
                    _dict.update({key.strip(): value.strip()})
                else:
                    value += line.strip()
                    _dict.update({key: value})
            else:
                yield _dict
                _dict = {}

        #return result

def match_email(emails, maintainer_emails=[]):
    for email in emails:
        for _email in maintainer_emails:
            if re.match(_email, email):
                return True
    return False


if __name__ == "__main__":
    DEEPIN_MAINTAINER = [ '[a-zA-Z0-9]*@linuxdeepin.com', 'leaeasy@gmail.com' ]
    pattern = re.compile('\<(.*)\>')
    for section in ['main', 'contrib', 'non-free']:
        print("# %s ----------------- "  % section)
        _deepin = "/mnt/mirror-snapshot/deepin-2015-process/dists/unstable/%s/source/Sources.gz" % section
        deepin = Packages(_deepin)
        _debian = "/srv/pool/www/debian/dists/unstable/%s/source/Sources.gz" % section
        debian = Packages(_debian)

        debian_packages = []
        deepin_packages = []
        for package in deepin.parse_packages():
            _package_name = package.get('Package')
            if package.get('Maintainer'):
                emails = pattern.findall(package.get('Maintainer'))
                if not match_email(emails, DEEPIN_MAINTAINER):
                    deepin_packages.append(package)
        for package in debian.parse_packages():
            debian_packages.append(package.get('Package'))

        for package in deepin_packages:
            if package.get('Package') not in debian_packages:
                print(package.get('Package'), package.get('Version'))
