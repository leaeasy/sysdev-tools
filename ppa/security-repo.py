#!/usr/bin/env python

from reprepro.repo import Repository
import os

BASE_PATH = "/srv/pool/security"

class UpdatePool(Repository):
    def __init__(self, repo_path, name=None):
        super().__init__(repo_path, name)

    def initial(self):
        super().initial(distros=['unstable'], arches=['i386','amd64','source'], repo_key='debian-mirror@linuxdeepin.com')

p = UpdatePool(os.path.join(BASE_PATH, '2015'))
p.initial()
