#! /usr/bin/env python
# -*- coding: utf-8 -*-
#

import re

def core_version(version):
    return re.sub("(.*)-s\d{10}~\w*", "\\1", version)

def is_substantial_version_change(v1, v2):
    cv1 = core_version(v1)
    cv2 = core_version(v2)
    return cv1 != cv2
