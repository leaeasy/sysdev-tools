#! /usr/bin/env python
# -*- coding: utf-8 -*-
#

from optparse import OptionParser
import os
import sys
from reprepro.repo import Repository

parser = OptionParser()
parser.add_option("--repo-name", dest="repo_name")
parser.add_option("--dist", dest="dist")
parser.add_option("--repo-path", dest="repo_path", default="/srv/pool")
parser.add_option("--incoming-path", dest="incoming_path", default="/srv/pool/incoming/ppa")

(options, args) = parser.parse_args()

incomings = []
for incoming in args:
    _incoming=os.path.join(options.incoming_path, incoming)
    if os.path.exists(os.path.join(options.incoming_path, incoming)):
        incomings.append(_incoming)

repo_path = os.path.join(options.repo_path,'base', options.repo_name)
repo = Repository(repo_path=repo_path)

if not os.path.exists(os.path.join(repo_path, 'conf/distributions')):
    repo.initial(distros=['experimental'], arches=['i386', 'amd64', 'source'], repo_key='debian-mirror@linuxdeepin.com', option_outdir=os.path.join(options.repo_path, 'www', options.repo_name))

repo.add_package(options.dist, incomings)
