#! /usr/bin/env python2
# -*- coding: utf-8 -*-
import os

class Mirror():
    def __init__(self, path):
        self.path = path

    def dists(self):
        dists = []
        for dist in os.listdir(os.path.join(self.path, 'dists')):
            _dist = os.path.join(self.path, 'dists', dist)
            if os.path.isdir(_dist) and os.path.exists(os.path.join(_dist, 'Release')):
                dists.append(dist) 
        return dists

    def sections(self, dist):
        with open(os.path.join(self.path, 'dists', dist, 'Release')) as fp:
            for line in fp.readlines():
                if line.strip().startswith("Components: "):
                    sections = line.strip().replace("Components: ", "").split()
                    break
        return sections

    def binaries(self, dist, section):
        binaries = []
        _section = os.path.join(self.path, 'dists', dist, section)
        if os.path.isdir(_section):
            for folder in os.listdir(_section):
                if folder == 'source':
                    binaries.append(folder)
                if folder.startswith('binary-'):
                    binaries.append(folder.replace('binary-',''))
        return binaries

    def pkg_info(self, dist, section, arch):
        if arch != "source":
            arch = "binary-%s" % arch
            _prefix = "Packages"
        else:
            _prefix = "Sources"

        _arch = os.path.join(self.path,'dists', dist, section, arch)
        _stores = os.listdir(_arch)

        for _suffix in [ '.gz', '.xz', '']:
            if _prefix+_suffix in _stores:
                content = _prefix + _suffix
                break
        
        _content = os.path.join(_arch, content)
        if _content.endswith('.gz'):
            import gzip
            fp = gzip.open(_content, 'rb')
        elif _content.endswith('.xz'):
            import lzma
            fp = lzma.open(_content, 'rb')
        else:
            fp = open(_content, 'r')

        result = {}
        try:
            for line in fp.readlines():
                flag = False
                if line.startswith("Package: "):
                    p = line.strip().replace("Package: ", "")
                if line.startswith("Version: "):
                    v = line.strip().replace("Version: ", "")
                    flag = True
                if flag:
                    result[p] = str(v)
        finally:
            fp.close()

        return result
