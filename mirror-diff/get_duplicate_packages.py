#! /usr/bin/env python2
# -*- coding: utf-8 -*-
#
# 获取仓库相同的软件包

from mirror import Mirror
import subprocess
import sys
from datetime import datetime

def compare_version(orig_ver, dest_ver):
    try:
        return subprocess.check_call(['dpkg', '--compare-versions', orig_ver, 'gt', dest_ver])
    except subprocess.CalledProcessError:
        return False


if __name__ == "__main__":
    if len(sys.argv) == 1:
        path = '/mnt/mirror-snapshot/deepin-2015-process/'
    else:
        path = sys.argv[1] 

    universe = Mirror(path=path)

    try:
        fp = open("result-%s" % datetime.now().strftime("%Y%m%d"), "w")
        for dist in universe.dists():
            sections = universe.sections(dist=dist)
            binaries = universe.binaries(dist=dist, section=sections[0])
        
            while len(sections) > 1:
                orig_section = sections[0]
                dest_sections = sections[1:]
                fp.write("#   %s\n" % orig_section)
                for binary in binaries:
                    fp.write(" %s\n" % binary)
                    for dest_section in dest_sections:
                        orig_info = universe.pkg_info(dist, orig_section, binary)
                        dest_info = universe.pkg_info(dist, dest_section, binary)
                        for pkg in orig_info:
                            if dest_info.get(pkg):
                                orig_ver = orig_info.get(pkg)
                                dest_ver = dest_info.get(pkg)
                                if not compare_version(orig_ver, dest_ver):
                                    fp.write("!Remove %s %s" % (pkg, orig_ver))
                                fp.write("    %s: %s %s %s\n" % (dest_section, pkg, orig_ver, dest_ver))
        
                sections.remove(orig_section)
    finally:
        fp.close()
