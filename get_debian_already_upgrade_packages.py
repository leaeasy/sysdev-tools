#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Get debian packages already removed.
import os
import io
import subprocess

class ParseError(Exception):
    def __init__(self, line):
        self._line = line

    def __str__(self):
        return "Could not parse changelog: " + self.line

class PackagesError(Exception):
    pass

class PackageBlock(object):
    def __init__(self):
        pass


EXTS_HANDLER = {'gz': 'gzip -d --stdout',
        'xz': 'unxz --stdout', 
        'bz2': 'bzip -d --stdout'}

class Packages(object):
    def __init__(self, file, endcoding='utf-8'):
        if not os.path.exists(file):
            raise PackagesError("No %s exists" % file)
        self.__file = file

    def __read(self):
        extension = os.path.splitext(self.__file)[1][1:]
        if extension in EXTS_HANDLER:
            try:
                import subprocess
                import signal
                _command = EXTS_HANDLER[extension].split()
                proc = subprocess.Popen(_command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=False,
                        preexec_fn=lambda: signal.signal(signal.SIGPIPE, signal.SIG_DFL))
            except (OSError, ValueError) as e:
                raise PackagesError(e)

            with open(self.__file) as fp:
                data = proc.communicate(fp.read())[0]

            if proc.returncode != 0:
                raise PackagesError("command has failed with code '%s'" % proc.returncode)

            buffer = io.BytesIO(data)
            return buffer

        else:
            raise PackagesError("%s read failed" % self.__file)

    def parse_packages(self):
        buffer = self.__read()
        _dict = {}
        for line in buffer.readlines():
            if line.strip() != "":
                if not line.startswith(" "):
                    key, value = line.strip().split(':', 1)
                    key = key.strip()
                    value = value.strip()
                    _dict.update({key.strip(): value.strip()})
                else:
                    value += line.strip()
                    _dict.update({key: value})
            else:
                yield _dict
                _dict = {}

def compare_version(orig_ver, dest_ver):
    try:
        return subprocess.check_call(['dpkg', '--compare-versions', orig_ver, 'gt', dest_ver])
    except subprocess.CalledProcessError:
        return False


if __name__ == "__main__":
    for section in ['main', 'contrib', 'non-free']:
        print("# %s ----------------- "  % section)
        _deepin = "/srv/pool/www/ppa/gnome320/dists/unstable/%s/source/Sources.gz" % section
        deepin = Packages(_deepin)
        _debian = "/srv/pool/www/debian/dists/unstable/%s/source/Sources.gz" % section
        debian = Packages(_debian)

        result = []
        for deepin_package in deepin.parse_packages():
            for debian_package in debian.parse_packages():
                if deepin_package.get('Package') == debian_package.get('Package'):
                    print(deepin_package.get('Package'), deepin_package.get('Version'), debian_package.get('Version'))
                    if compare_version(deepin_package.get('Version'), debian_package.get('Version')):
                        result.append(deepin_package.get('Package'))

        print(result)

